class DegreePlan < ActiveRecord::Base
  extend FriendlyId
  friendly_id :sluggified_url, use: :slugged
  serialize :plan, JSON

  validates :name, presence: true, uniqueness: {scope: [:degree_type, :degree_awarded]}
  validates :degree_type, presence: true
  validates :degree_awarded, presence: true
  validates :slug, presence: true, uniqueness: true

  def sluggified_url
    "#{name} #{degree_awarded} #{degree_type}"
  end
end
