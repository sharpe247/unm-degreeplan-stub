json.array!(@degree_plans) do |degree_plan|
  json.extract! degree_plan, :id, :name, :degree_type, :degree_awarded
  json.url degree_plan_url(degree_plan, format: :json)
end
