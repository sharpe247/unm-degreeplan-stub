# This app is meant to work as a simple API stub for degree plans

## To set up

run these commands:

`
rake db:setup
`
`
rails s
`

Go to http://localhost:3000/degree_plans.json

This will give you all of the information needed to access all degree plans 
