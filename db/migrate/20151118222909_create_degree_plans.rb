class CreateDegreePlans < ActiveRecord::Migration
  def change
    create_table :degree_plans do |t|
      t.string :name
      t.string :degree_type
      t.string :degree_awarded
      t.string :slug
      t.text :plan

      t.timestamps null: false
    end

    add_index :degree_plans, :slug
  end
end
