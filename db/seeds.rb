# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Dir["#{Rails.root.join("db", "degree_plans").to_s}/*.json"].each do |json_file|
  plan_hash = JSON.parse(File.read(json_file))

  degree_plan = DegreePlan.find_or_create_by(
    name: plan_hash["name"],
    degree_type: plan_hash["degree_type"],
    degree_awarded: plan_hash["degree_awarded"]
  )

  degree_plan.update_attributes(plan: plan_hash)
end
